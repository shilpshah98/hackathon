package com.training.FinanceApp.entities;

import java.time.LocalDate;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {
    public enum Status { 
        CREATED, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLYFILLED, ERROR
    }
    
    @Id
    private ObjectId objectId;
    private Calendar dateCreated;
    private Status status = Status.CREATED;
    private String ticker;
    private int quantity;
    private double requestedPrice;

    
    public Trade(Calendar dateCreated, String ticker, int quantity, double requestedPrice, Status status) {
        this.dateCreated = dateCreated;
        this.ticker = ticker;
        this.quantity = quantity;
        this.requestedPrice = requestedPrice;
        this.status = status;
    }


    public Trade() {
	}


	public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public Calendar getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Calendar dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}