import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class RequestService {
  private requestUrl: string;

  constructor(private http: HttpClient) {
    this.requestUrl = 'localhost:8080/trades';
  }
  
}
