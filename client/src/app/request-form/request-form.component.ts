import { Component, OnInit } from '@angular/core';
import { RequestService } from '../request/request.service';
import { FormControl } from '@angular/forms';
import { HttpBackend } from '@angular/common/http';

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.css']
})
export class RequestFormComponent {

  ticker = new FormControl();
  quantity = new FormControl();
  price = new FormControl();

  constructor(private requestService: RequestService) { }

  
  onSubmit(){
   // this.requestService.getEntry(this.ticker.value, this.quantity.value, this.price.value);
    //let url = "http://localhost:4200/hello";
    //RequestTrade(url);
  }
}