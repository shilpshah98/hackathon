import { Component, OnInit } from '@angular/core';
import { TradeService } from '../trade/trade.service';
import {Sort} from '@angular/material/sort';


@Component({
  selector: 'app-trade-list',
  templateUrl: './trade-list.component.html',
  styleUrls: ['./trade-list.component.css']
})
export class TradeListComponent implements OnInit {

  trades:Array<any>;
  constructor(private tradeService: TradeService) { }

  ngOnInit() {
    this.tradeService.getAll().subscribe(data => {
      this.trades = data;
    });
  }

  /*
  user_ticker:String;
  user_quantity:String;
  user_requestPrice:String;
  */

}
